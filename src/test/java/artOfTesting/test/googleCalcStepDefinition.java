package artOfTesting.test;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.api.java.en.And;
import static org.junit.Assert.*;


public class googleCalcStepDefinition {
	
	
	protected WebDriver driver;
	
	 @Before
	    public void setup() {
	    //   driver = new FirefoxDriver();
	    //	 System.setProperty("webdriver.ie.driver", "c://IEDriverServer_x64_2.46.0/IEDriverServer.exe");
	    //      driver=new InternetExplorerDriver(); 
		System.setProperty("webdriver.chrome.driver", "c://qiu/workspace/cucumberJava/chromedriver.exe");
		driver = new ChromeDriver();
	      
	}
		

     @Given("the user is on Fund Transfer Page")
        public void The_user_is_on_the_fund_transfer_page(){
        driver.get("http://dl.dropbox.com/u/55228056/fundTransfer.html");
      }


      @When("he enters \"([^\"]*)\" as payee name")
        public void He_enters_payee_name(String payeeName){
        driver.findElement(By.id("payee")).sendKeys(payeeName);
      }


       @And("he enters \"([^\"]*)\" as amount")
       public void He_enters_amount(String amount){
       driver.findElement(By.id("amount")).sendKeys(amount);
       }

       @And("he submits request for Fund Transfer")
       public void He_submits_request_for_fund_transfer(){
       driver.findElement(By.id("transfer")).click();
       }


       @Then("ensure the fund transfer is complete with \"([^\"]*)\" message")
       public void Ensure_the_fund_transfer_is_complete(String msg){
        WebElement message = driver.findElement(By.id("message"));
     //   System.out.println(message.getText());
     //   System.out.println(msg);
        assertEquals(message.getText(),msg);
         }

       @After
       public void CloseBrowser(){
         driver.close();
         driver.quit();
       }

}


